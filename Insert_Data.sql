-- Version 3: ADD Gesamtspreis to 
-- Insert Ernährungskategorien
INSERT INTO Ernährungskategorien (Name)
VALUES
('Fleisch & Gemüse'),
('Fisch & Gemüse'),
('Vegetarisch'),
('Vegan');


-- Insert Rezept
INSERT INTO Rezept (Name, Zeit, Bewertung, ErnährungskategorienNr)
VALUES
('Lachslasagne', 70, 5, 302),
('Thaicurry mit Hähnchen', 120, 4, 301),
('Kartoffelsuppe', 60, 3, 303),
('Milchreis mit Apfelmus', 70, 3, 304),
('Sommerlicher Couscous-Salat', 20, 4, 303);




-- Insert Allergene
INSERT INTO Allergene (Name)
VALUES
('Laktose'),
('Gluten'),
('Erdnuss'),
('Ei'),
('Soja'),
('Fisch'),
('Weizen'),
('Stärke'),
('Karotte');


-- Insert Rezept_Allergene

INSERT INTO Rezept_Allergene (RezeptNr, AllergeneNr)
VALUES
(1, 401),
(1, 407),
(1, 402),
(2, 406),
(2, 402),
(3, 408),
(3, 409),
(4, 401);


-- Insert Zutat
INSERT INTO Zutat (Bezeichnung, Bestand, Nettopreis, Einheit, Kohlenhydrate_pro_100g, Kalorien_pro_100g, Fett_pro_100g)
VALUES
('Milch', 1, 0.70, 'L', 4.8, 61, 3.3),
('Weißbrot', 500, 1.00, 'g', 50, 265, 3),
('Erdnuss', 100, 2.00, 'g', 16.1, 567, 49.2),
('Ei', 1, 0.20, 'Stk.', 0.6, 72, 5.6),
('Sojabohne', 100, 1.50, 'g', 15, 364, 20.1),
('Fisch', 500, 3.00, 'g', 0, 82, 0.5),
('Mehl', 1, 0.50, 'kg', 71, 354, 1),
('Stärke', 50, 1.00, 'g', 88, 347, 0),
('Karotte', 500, 0.50, 'g', 9.6, 41, 0.2),
('Hühnerbrust', 500, 2.50, 'g', 0, 165, 3.6),
('Kartoffel', 1, 1.00, 'kg', 17, 77, 0.1),
('Tomate', 500, 1.50, 'g', 3.9, 18, 0.2),
('Salat', 1, 0.50, 'Stk.', 1.4, 10, 0.1),
('Zwiebel', 1, 0.20, 'Stk.', 9.3, 40, 0.1),
('Knoblauch', 1, 0.30, 'Stk.', 24.7, 149, 0.3),
('Spinat', 100, 1.00, 'g', 3.6, 23, 0.4),
('Paprika', 1, 1.00, 'Stk.', 6, 40, 0.2),
('Gurke', 1, 0.50, 'Stk.', 0.7, 15, 0.1),
('Banane', 1, 1.00, 'Stk.', 22.8, 95, 0.3),
('Apfel', 1, 1.00, 'Stk.', 10.4, 52, 0.2),
('Orange', 1, 1.00, 'Stk.', 8.9, 47, 0.1),
('Erdbeere', 250, 2.00, 'g', 5.6, 32, 0.3),
('Himbeere', 250, 2.50, 'g', 5.3, 34, 0.5),
('Blaubeere', 250, 3.00, 'g', 14.5, 57, 0.3),
('Ananas', 1, 2.00, 'Stk.', 9.6, 50, 0.2),
('Mango', 1, 2.50, 'Stk.', 15.2, 67, 0.4),
('Kiwi', 1, 1.50, 'kg', 9.1, 61, 0.6),
('Pfirsich', 1, 1.50, 'kg', 9.5, 39, 0.2),
('Birne', 1, 1.00, 'Stk.', 10.3, 57, 0.1),
('Traube', 500, 2.00, 'g', 16, 69, 0.2),
('Wassermelone', 1, 1.50, 'Stk.', 7.6, 30, 0.2),
('Brokkoli', 500, 1.50, 'g', 6.6, 34, 0.4),
('Rosenkohl', 500, 2.00, 'g', 4, 40, 0.3),
('Lauch', 1, 0.50, 'Stk.', 1.5, 20, 0.1),
('Sellerie', 1, 0.50, 'Stk.', 2.1, 14, 0.1),
('Mais', 1, 0.50, 'Stk.', 19, 86, 1.5),
('Avocado', 1, 1.50, 'Stk.', 2, 160, 15.3),
('Granatapfel', 1, 2.00, 'Stk.', 14.4, 72, 1.2),
('Feige', 1, 2.00, 'Stk.', 16.2, 74, 0.3),
('Dattel', 100, 2.50, 'g', 75, 282, 0.4),
('Mandarine', 1, 1.00, 'kg', 9.3, 53, 0.3),
('Grapefruit', 1, 1.50, 'Stk.', 9.7, 41, 0.2),
('Limette', 1, 0.50, 'Stk.', 4.2, 16, 0.1),
('Zitrone', 1, 0.50, 'Stk.', 3, 29, 0.1),
('Aprikose', 1, 1.00, 'Stk.', 9.3, 48, 0.1),
('Kirsche', 250, 2.00, 'g', 16.5, 63, 0.3),
('Pflaume', 250, 1.00, 'g', 11.4, 47, 0.2),
('Brombeere', 250, 2.50, 'g', 5.3, 34, 0.4),
('Holunderbeere', 250, 2.50, 'g', 6.2, 37, 0.4),
('Preiselbeere', 250, 2.50, 'g', 4.6, 27, 0.1),
('Lasagneplatten', 500, 1.00, 'g', 70, 354, 2.5),
('Minze', 100, 1.50, 'g', 3.6, 23, 0.4),
('Lachsfilet', 250, 4.00, 'g', 0, 207, 13.6),
('Butter', 50, 0.50, 'g', 0.3, 217, 11.1),
('Brühe', 1, 0.50, 'l', 2, 8, 0.1),
('Sahne', 200, 0.50, 'ml', 4.8, 185, 12.5),
('Parmesan', 100, 2.00, 'g', 2.5, 392, 27.2),
('Zitrone', 1, 0.50, 'Stück', 9.3, 29, 0.3),
('Pfeffer', 100, 0.50, 'g', 26, 286, 2.8),
('Muskat', 50, 1.00, 'g', 41, 341, 3.3),
('Reis', 500, 0.70, 'g', 78, 354, 0.9),
('Sojasauce', 250, 1.00, 'ml', 10, 74, 0.5),
('Zucker', 1, 0.50, 'EL', 99.8, 387, 0),
('Knoblauchzehe', 1, 0.10, 'Stück', 2.7, 149, 0.3),
('Gemischtes Gemüse', 500, 1.50, 'g', 3.6, 25, 0.4),
('Bambussprossen', 250, 1.50, 'g', 4, 21, 0.2),
('Kokosmilch', 500, 1.50, 'ml', 2, 190, 17),
('Currypaste', 100, 2.00, 'g', 5, 113, 6.7),
('Lauch', 1, 0.50, 'Stück', 1.5, 20, 0.1),
('Milchreis', 500, 0.80, 'g', 75, 345, 1.5),
('Vanillezucker', 1, 0.30, 'Packung', 80, 320, 0),
('Apfelmus', 500, 0.50, 'g', 14, 57, 0.2),
('Couscous', 250, 1.00, 'g', 72, 330, 0.6),
('Gemüsebrühe', 100, 0.10, 'ml', 4, 12, 0.1),
('Zucchini', 100, 0.50, 'g', 3.6, 18, 0.3),
('Petersilie', 1, 0.50, 'Bund', 2, 32, 0.7),
('Oregano', 1, 0.20, 'TL', 7, 24, 0.6),
('Pflanzenöl', 250, 1.00, 'ml', 10, 900, 100),  
('Salz', 1, 0.50, 'TL', 0, 0, 0),
('Wiener würstchen', 250, 2.50, 'g', 2, 273, 22),
('Schittlauch', 100, 0.50, 'g', 1.5, 20, 0.1),
('majoran', 100, 0.50, 'g', 3.6, 23, 0.4),
('Käse', 250, 1.50, 'g', 16.1, 567, 49.2);

-- Insert Rezept_Zutat
INSERT INTO Rezept_Zutat (ZutatID, RezeptNr, Quantität)
VALUES
(20051, 1, 1),
(20016, 1, 4),
(20053, 1, 2),
(20083, 1, 1),
(20054, 1, 1),
(20007, 1, 1),
(20001, 1, 1),
(20055, 1, 1),
(20057, 1, 1),
(20006, 1, 1),
(20044, 1, 1),
(20059, 1, 1),
(20060, 1, 1),
(20010, 1, 1),
(20014, 1, 1),
(20010, 2, 1),
(20061, 2, 1),
(20062, 2, 2),
(20063, 2, 1),
(20064, 2, 1),
(20065, 2, 2),
(20066, 2, 1),
(20067, 2, 1),
(20068, 2, 1),
(20059, 2, 1),
(20011, 3, 1),
(20009, 3, 1),
(20034, 3, 1),
(20014, 3, 1),
(20074, 3, 10),
(20080, 3, 1),
(20076, 3, 1),
(20059, 3, 1),
(20081, 3, 1),
(20082, 3, 1),
(20060, 3, 1),
(20001, 4, 1),
(20070, 4, 1),
(20063, 4, 4),
(20054, 4, 1),
(20071, 4, 1),
(20072, 4, 1),
(20073, 5, 1),
(20074, 5, 3),
(20012, 5, 1),
(20075, 5, 1),
(20017, 5, 1),
(20076, 5, 1),
(20077, 5, 1),
(20078, 5, 2),
(20079, 5, 1),
(20059, 5, 1);


-- Insert Zahlungsmethode
INSERT INTO Zahlungsmethode (Name)
VALUES
('Bar'),
('EC'),
('Überweisung'),
('Paypal'),
('Klarna'),
('Online Banking');



-- Insert Lieferant
INSERT INTO Lieferant (Name, Strasse, HausNr, PLZ, Stadt, Telefon)
VALUES
('Marcel Schmidt', 'Bäckerstraße', 7, '20095', 'Hamburg', '040634637'),
('Luka Mayer', 'Schillerstraßestraße', 9, '10115', 'Berlin', '0553267867'),
('Leon Schonlau', 'Mahlerstraßestraße', 6, '28195', 'Bremen', '068346588'),
('Sascha Fernandez', 'Hauptstraßestraße', 2, '80331', 'München', '073346699'),
('Marie Draxler', 'Bahnhofstraße', 56, '50667', 'Köln', '082346794'),
('Linn Lee', 'Schlossalle', 1, '30159', 'Hannover', '092346347');




-- Insert Kunde
INSERT INTO Kunde (Vorname, Nachname, Geschlecht, Geburtsdatum, Strasse, HausNr, PLZ, Stadt, Email, Telefon, Status)
VALUES
('Marcel', 'Schmidt', 'Männlich', '2001-12-12', 'Bäckerstraße', 7, '20095', 'Hamburg', 'Marcel@schmidt.com', '040634637', 'Aktiv'),
('Luka', 'Mayer', 'Männlich', '2003-02-17', 'Schillerstraße', 9, '10115', 'Berlin', 'Luka@Mayer.com', '03053267867', 'Aktiv'),
('Leon', 'Schonlau', 'Männlich', '1999-04-05', 'Mahlerstraße', 6, '28195', 'Bremen', 'Leon@schonlau.com', '04218346588', 'Gesperrt'),
('Sascha', 'Fernandez', 'Männlich', '2003-06-06', 'Hauptstraße', 2, '80331', 'München', 'Sascha@fernandez.com', '0893346699', 'Aktiv'),
('Marie', 'Draxler', 'Weiblich', '2001-03-20', 'Bahnhofstraße', 56, '50667', 'Köln', 'Mari@draxler.com', '082346794', 'Inaktiv'),
('Linn', 'Lee', 'Weiblich', '2004-11-13', 'Schlossalle', 1, '30159', 'Hannover', 'Linn@lee.com', '02212346347', 'Aktiv'),
('Marlon', 'Macher', 'Männlich', '2002-10-18', 'Münchenerstraße', 5, '30159', 'Berlin', 'Marlon@macher.com', '03065872398', 'Inaktiv'),
('Johannah', 'Kramer', 'Weiblich', '1990-12-30', 'Lessingstraße', 34, '50667', 'Köln', 'Johannah@kramer.com', '08982542683', 'Inaktiv'),
('Willy', 'Friede', 'Männlich', '1989-12-11', 'Hafenstraße', 10, '80334', 'München', 'Willy@Friede.com', '08998542558', 'Aktiv');




-- Insert Bestellung
INSERT INTO Bestellung (Gesamtpreis, Datum, Bemerkung, KdNr, ZahlungsmethodeNr, LiefNr)
VALUES
(52.80, '2023-02-05 11:14:32', 'Abholung im Geschäft', 1001, 501, 601),
(28.60, '2023-02-04 13:14:45', 'Bitte vorher anrufen', 1002, 502, 602),
(26.10, '2023-02-05 20:19', NULL, 1008, 503, 603),
(19.20, '2023-02-02 23:15:14', 'Bitte beim Mustermann klingeln', 1005, 504, 604),
(40.00, '2023-02-02 16:00:12', 'Bin erst ab 14:00 erreichbar', 1007, 505, 605),
(158.40, '2023-02-02 03:14:14', NULL, 1008, 506, 601);



-- Insert Bestellung_Rezept
INSERT INTO Bestellung_Rezept (BestellNr, RezeptNr, Portionen)
VALUES
(10001, 1, 2),
(10002, 2, 2),
(10003, 3, 3),
(10004, 4, 4),
(10005, 5, 5),
(10006, 1, 6);