

-- Navid
-- Berechnung des Rezeptpreises
    SELECT
        R.RezeptNr,
        R.Name AS RezeptName,
        SUM(Z.Nettopreis * RZ.Quantität) AS Gesamtpreis
    FROM
        Rezept R
    LEFT JOIN
        Rezept_Zutat RZ ON R.RezeptNr = RZ.RezeptNr
    LEFT JOIN
        Zutat Z ON RZ.ZutatID = Z.ZutatID
    GROUP BY
        R.RezeptNr, R.Name;


-- Berechnung der Gesamtpreis
SELECT
    BR.BestellNr,
    R.RezeptNr,
    R.Name AS RezeptName,
    BR.Portionen,
    SUM(Z.Nettopreis * RZ.Quantität * BR.Portionen) AS Gesamtpreis
FROM
    Bestellung_Rezept BR
JOIN
    Rezept R ON BR.RezeptNr = R.RezeptNr
LEFT JOIN
    Rezept_Zutat RZ ON R.RezeptNr = RZ.RezeptNr
LEFT JOIN
    Zutat Z ON RZ.ZutatID = Z.ZutatID
GROUP BY
    BR.BestellNr, R.RezeptNr, R.Name, BR.Portionen;


-- Auswahl aller Zutaten, die bisher keinem Rezept zugeordnet sind
SELECT Z.Bezeichnung, Z.ZutatID, RZ.RezeptNr
FROM Zutat Z
LEFT JOIN Rezept_Zutat RZ ON Z.ZutatID = RZ.ZutatID
WHERE RZ.RezeptNr IS NULL;




--Auswahl aller Rezepte mit der Menge der Zutaten
SELECT
    R.RezeptNr,
    R.Name AS Rezeptueberschrift,
    COUNT(RZ.ZutatID) AS AnzahlZutaten
FROM
    Rezept R
JOIN
    Rezept_Zutat RZ ON R.RezeptNr = RZ.RezeptNr
GROUP BY
    R.RezeptNr, R.Name
ORDER BY
    RezeptNr;


-- Farshat
-- Auswahl aller Rezepte, die weniger als zwölf Zutaten enthalten
SELECT R.RezeptNr, R.Name as Rezeptueberschrift
FROM Rezept R
JOIN Rezept_Zutat RZ ON R.RezeptNr = RZ.RezeptNr
GROUP BY R.RezeptNr, R.Name 
HAVING COUNT(RZ.ZutatID) < 12;


-- Auswahl aller Rezepte, die weniger als zwölf Zutaten enthalten und eine bestimmte Ernährungskategorie erfüllen
SELECT R.RezeptNr, R.Name
FROM Rezept R
JOIN Rezept_Zutat RZ ON R.RezeptNr = RZ.RezeptNr
WHERE R.ErnährungskategorienNr = 301
GROUP BY R.RezeptNr, R.Name
HAVING COUNT(RZ.ZutatID) < 12;


-- Auswahl aller Rezepte einer bestimmten Ernährungskategorie (Vegetarisch)
SELECT Rezept.Name
FROM Rezept
JOIN Ernährungskategorien ON Rezept.ErnährungskategorienNr = Ernährungskategorien.ErnährungskategorienNr
WHERE Ernährungskategorien.Name = 'Vegetarisch';


-- Babur
-- Berechnung der durchschnittlichen Nährwerte aller Bestellungen eines Kunden

SELECT AVG(Kalorien_pro_100g) AS Durchschnittliche_Kalorien,
       AVG(Kohlenhydrate_pro_100g) AS Durchschnittliche_Kohlenhydrate,
       AVG(Fett_pro_100g) AS Durchschnittliche_Fett
FROM Kunde
JOIN Bestellung ON Kunde.KdNr = Bestellung.KdNr
JOIN Bestellung_Rezept ON Bestellung.BestellNr = Bestellung_Rezept.BestellNr
JOIN Rezept ON Bestellung_Rezept.RezeptNr = Rezept.RezeptNr
JOIN Rezept_Zutat ON Rezept.RezeptNr = Rezept_Zutat.RezeptNr
JOIN Zutat ON Rezept_Zutat.ZutatID = Zutat.ZutatID
WHERE Kunde.KdNr = 1001; -- Hier die tatsächliche Kunden-ID einfügen



-- durchschnittliche Anzahl der Portionen für jedes Rezept zu berechnen
SELECT Rezept.RezeptNr, Rezept.Name,
       (SELECT AVG(Portionen) FROM Bestellung_Rezept WHERE Bestellung_Rezept.RezeptNr = Rezept.RezeptNr) AS Durchschnittliche_Portionen
FROM Rezept;


-- Vincent
--Auswahl aller Rezepte, die eine gewisse Zutat enthalten (Tomaten):
SELECT Rezept.RezeptNr, Rezept.Name
FROM Rezept
INNER JOIN Rezept_Zutat ON Rezept.RezeptNr = Rezept_Zutat.RezeptNr
INNER JOIN Zutat ON Rezept_Zutat.ZutatID = Zutat.ZutatID
WHERE Zutat.Bezeichnung = 'Tomate';


--Auswahl aller Zutaten die in Rezept “Kartoffelsuppe” vorhanden sind:
SELECT Zutat.Bezeichnung 
FROM Rezept 
JOIN Rezept_Zutat ON Rezept.RezeptNr = Rezept_Zutat.RezeptNr
JOIN Zutat ON Rezept_Zutat.ZutatID = Zutat.ZutatID
WHERE Rezept.Name = 'Kartoffelsuppe';


-- Auswahl aller Zutaten, die einem bestimmten Rezept zugeordnet sind
SELECT Z.Bezeichnung, Z.ZutatID, RZ.RezeptNr
FROM Zutat Z
LEFT JOIN Rezept_Zutat RZ ON Z.ZutatID = RZ.ZutatID
WHERE RZ.RezeptNr = 2;
