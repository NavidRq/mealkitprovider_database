-- Version 3: Deleted NOT NULL from Gesamtpreis AND edited LiefNr AND edited AllergeneNr so it can be NULL
-- Create Rezept-Table
CREATE TABLE Rezept (
      RezeptNr INT AUTO_INCREMENT,
      Name VARCHAR(30) NOT NULL,
      Zeit INT NOT NULL,
      Bewertung INT CHECK (Bewertung >= 0 AND Bewertung <= 5),
      ErnährungskategorienNr INT,
      PRIMARY KEY(RezeptNr)
  );



-- Create Ernährungskategorien-Table
CREATE TABLE Ernährungskategorien(
      ErnährungskategorienNr INT AUTO_INCREMENT,
      Name VARCHAR(30) NOT NULL,
      PRIMARY KEY(ErnährungskategorienNr)
  ) AUTO_INCREMENT=301;


-- Add FK to Rezept-Table
  ALTER TABLE Rezept
  ADD FOREIGN KEY(ErnährungskategorienNr)
  REFERENCES Ernährungskategorien(ErnährungskategorienNr)
  ON DELETE SET NULL;


-- Create Allergene-Table
CREATE TABLE Allergene(
      AllergeneNr INT AUTO_INCREMENT,
      Name VARCHAR(30) NOT NULL,
      PRIMARY KEY(AllergeneNr)
  ) AUTO_INCREMENT=401;


-- Create Rezept_Allergene-Table
CREATE TABLE Rezept_Allergene(
      RezeptNr INT NOT NULL,
      AllergeneNr INT,
      PRIMARY KEY(RezeptNr, AllergeneNr),
      FOREIGN KEY(RezeptNr) REFERENCES Rezept(RezeptNr) ON DELETE CASCADE,
      FOREIGN KEY(AllergeneNr) REFERENCES Allergene(AllergeneNr) ON DELETE CASCADE
  );




CREATE TABLE Zahlungsmethode (
      ZahlungsmethodeNr INT NOT NULL AUTO_INCREMENT,
      Name VARCHAR(255) NOT NULL,
      PRIMARY KEY (ZahlungsmethodeNr)
  ) AUTO_INCREMENT=501;



CREATE TABLE Kunde (
      KdNr INT NOT NULL AUTO_INCREMENT,
      Vorname VARCHAR(30) NOT NULL,
      Nachname VARCHAR(30) NOT NULL,
      Geschlecht ENUM('Männlich', 'Weiblich', 'Divers'),
      Geburtsdatum DATE,
      Strasse VARCHAR(255) NOT NULL,
      HausNr INT NOT NULL,
      PLZ CHAR(5) NOT NULL, 
      Stadt VARCHAR(255) NOT NULL,
      Email VARCHAR(255) NOT NULL,
      Telefon VARCHAR(20),
      Status ENUM('aktiv', 'inaktiv', 'gesperrt'),
      PRIMARY KEY (KdNr)
  ) AUTO_INCREMENT=1001;



CREATE TABLE Lieferant (
      LiefNr INT NOT NULL AUTO_INCREMENT,
      Name VARCHAR(30) NOT NULL,
      Strasse VARCHAR(30) NOT NULL,
      HausNr INT NOT NULL,
      PLZ CHAR(5) NOT NULL,
      Stadt VARCHAR(30) NOT NULL,
      Telefon VARCHAR(20),
      PRIMARY KEY (LiefNr)
  ) AUTO_INCREMENT=601;


CREATE TABLE Bestellung (
      BestellNr INT NOT NULL AUTO_INCREMENT,
      Gesamtpreis DECIMAL(10,2),
      Datum DATETIME NOT NULL,
      Bemerkung VARCHAR(255),
      KdNr INT NOT NULL,
      ZahlungsmethodeNr INT NOT NULL,
      LiefNr INT NOT NULL,
      PRIMARY KEY (BestellNr),
      FOREIGN KEY (KdNr) REFERENCES Kunde (KdNr),
      FOREIGN KEY (ZahlungsmethodeNr) REFERENCES Zahlungsmethode (ZahlungsmethodeNr),
      FOREIGN KEY (LiefNr) REFERENCES Lieferant (LiefNr) ON DELETE CASCADE
  ) AUTO_INCREMENT=10001;

  CREATE TABLE Bestellung_Rezept (
      BestellNr INT NOT NULL,
      RezeptNr INT NOT NULL,
      Portionen INT NOT NULL,
      PRIMARY KEY (BestellNr, RezeptNr),
      FOREIGN KEY (BestellNr) REFERENCES Bestellung (BestellNr),
      FOREIGN KEY (RezeptNr) REFERENCES Rezept (RezeptNr)
  );



-- Zutaten-Tabelle erstellen
CREATE TABLE Zutat(
  ZutatID INT NOT NULL AUTO_INCREMENT,
  Bezeichnung VARCHAR(255) NOT NULL,
  Bestand INT,
  Nettopreis DECIMAL(10,2),
  Einheit VARCHAR(10),
  Kohlenhydrate_pro_100g DECIMAL(10,2),
  Kalorien_pro_100g DECIMAL(10,2),
  Fett_pro_100g DECIMAL(10,2),
  LiefNr INT,
  PRIMARY KEY (ZutatID)
) AUTO_INCREMENT=20001;




CREATE TABLE Bestellung_Zutat (
   BestellNr INT NOT NULL,
   ZutatID INT NOT NULL,
   Menge INT CHECK(Menge >= 1 AND Menge <= 100),
   PRIMARY KEY(BestellNr, ZutatID),
   FOREIGN KEY(BestellNr) REFERENCES Bestellung(BestellNr) ON DELETE CASCADE,
   FOREIGN KEY(ZutatID) REFERENCES Zutat(ZutatID) ON DELETE CASCADE
);


CREATE TABLE Rezept_Zutat (
  ZutatID INT NOT NULL,
  RezeptNr INT NOT NULL,
  Quantität INT CHECK (Quantität >= 1 AND Quantität <= 100),
  PRIMARY KEY(RezeptNr, ZutatID),
  FOREIGN KEY(RezeptNr) REFERENCES Rezept(RezeptNr) ON DELETE CASCADE,
  FOREIGN KEY(ZutatID) REFERENCES Zutat(ZutatID) ON DELETE CASCADE
);